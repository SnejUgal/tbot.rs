require 'rouge'

source = gets(nil)
formatter = Rouge::Formatters::HTML.new
lexer = Rouge::Lexers::Rust.new
puts formatter.format(lexer.lex(source))