const path = require(`path`);
const { execSync } = require('child_process');
const CopyPlugin = require('copy-webpack-plugin');

const mode = process.env.NODE_ENV || `production`;
const isProduction = mode === `production`;

const src = path.resolve(__dirname, `src`);
const name = filePath => {
  let name = path.relative(src, filePath);

  if (path.extname(name) === `.pug`) {
    name = `${path.basename(name, `.pug`)}.html`;
  }

  return name;
};
const extension = (...extensions) => filePath =>
  extensions.includes(path.extname(filePath));

const file = {
  loader: `file-loader`,
  options: {
    name,
  },
};

const extract = [file, `extract-loader`];

const pug = [
  file,
  `extract-loader`,
  {
    loader: `html-loader`,
    options: {
      attrs: [`link:href`, `:src`],
      ...(isProduction
        ? {
            minimize: true,
            conservativeCollapse: false,
          }
        : {}),
    },
  },
  { 
    loader: `pug-html-loader`,
    options: {
      filters: {
        rust: (text, _) => {
          return execSync(`ruby ${path.resolve(__dirname, 'highlight.rb')}`, {input: text}).toString();
        }
      }
    },
  },
];

const css = [
  ...extract,
  `css-loader`,
  {
    loader: `postcss-loader`,
    options: {
      plugins: [
        require(`postcss-nested`)(),
        require(`postcss-preset-env`)({
          stage: 0,
          features: {
            "nesting-rules": false,
          },
        }),
        require(`autoprefixer`)(),
        ...(isProduction ? [require(`postcss-clean`)] : []),
      ],
    },
  },
];

const config = {
  mode,
  entry: `./src/index.pug`,
  output: {
    filename: `bundle.js`,
    path: path.resolve(__dirname, `build`),
  },
  module: {
    rules: [
      {
        test: extension(`.pug`),
        use: pug,
      },
      {
        test: extension(`.css`),
        use: css,
      },
      {
        test: extension(`.svg`, `.webp`),
        use: [file],
      },
    ],
  },
  plugins: [
    new CopyPlugin([
      { from: 'src/images/og_image.png', to: 'images/og_image.png' },
    ]),
  ],
  devServer: {
    hot: true,
    compress: true,
    host: `0.0.0.0`,
    port: 3000,
  },
};

module.exports = config;
