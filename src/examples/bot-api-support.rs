use tbot::types::{chat, input_file::Audio};
use tokio::fs::read;

type Error = Box<dyn std::error::Error>;

#[tokio::main]
async fn main() -> Result<(), Error> {
  let bot = tbot::from_env!("BOT_TOKEN");
  let chat = chat::Id(..);
  let bytes = read("Rusty James.mp3").await?;
  let audio = Audio::bytes(&bytes);
  bot.send_audio(chat, audio).call().await?;

  Ok(())
}