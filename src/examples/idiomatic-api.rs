use tbot::{errors::MethodCall, types::chat};

#[tokio::main]
async fn main() -> Result<(), MethodCall> {
  let bot = tbot::from_env!("BOT_TOKEN");
  let chat = chat::Id(..);
  let chat = bot.get_chat(chat).call().await?;

  match chat.kind {
    chat::Kind::Group { title, .. } =>
      println!("I’m in {}", title),
    _ => println!("Oh, it wasn’t a group"),
  }

  Ok(())
}