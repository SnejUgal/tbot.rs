use tbot::prelude::*;
use tokio::sync::Mutex;

#[tokio::main]
async fn main() {
  let mut bot = tbot::from_env!("BOT_TOKEN")
    .stateful_event_loop(Mutex::new(0));

  bot.command("counter", |context, counter| {
    async move {
      let message = format!(
        "The counter is {} now",
        *counter.lock().await,
      );

      context
        .send_message(&message)
        .call()
        .await
        .unwrap();
    }
  });

  bot.polling().start().await.unwrap();
}
