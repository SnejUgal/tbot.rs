use tbot::prelude::*;
use tokio::signal::ctrl_c;
use futures::future::select;

#[tokio::main]
async fn main() {
  let mut bot = tbot::from_env!("BOT_TOKEN")
    .event_loop();

  bot.text(|context| async move { .. });

  let polling = bot.polling().timeout(60);
  select(
    Box::pin(polling.start()),
    Box::pin(ctrl_c()),
  ).await;

  // shut down gracefully...
}